FROM python:3.7-alpine
COPY ./src/requirements.txt /app/requirements.txt
RUN pip install -r /app/requirements.txt
COPY ./src /app
## TODO: Make this run as a non-root user
CMD ["python", "/app/app.py"]