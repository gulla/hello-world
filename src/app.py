from flask import Flask, render_template, redirect, url_for, request, session, flash, g, abort
from functools import wraps
import logging
from logging.handlers import RotatingFileHandler
import datetime

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST']) #this is called a decorator
def hello_world():
    return "hello world!" #open('static/logo.txt').read()

# Custom 404 page
@app.errorhandler(404)
def page_not_found(e):
#    application.logger.error('[ERROR] ' + request.remote_addr + "\t404\t" + request.url)
    return "oh noes 404" #render_template('404.html'), 404

if __name__ == '__main__':
    handler = RotatingFileHandler('/tmp/mooit.log', maxBytes=10000, backupCount=1)
    handler.setLevel(logging.INFO)
    #app.logger.addHandler(handler)
    app.run(debug=True, host='0.0.0.0')